var container = document.getElementById('canvas');
var renderer;
if (Detector.webgl) {
    Detector.addGetWebGLMessage();
    renderer = new THREE.WebGLRenderer({
        canvas: container,
        alpha: true
    });
} else {
    renderer = new THREE.CanvasRenderer();
}
var center = new THREE.Vector3();
var camera = new THREE.PerspectiveCamera(27, window.innerWidth / window.innerHeight, 1, 2000);
var scene = scene = new THREE.Scene();
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var controls;
var obj;
var modalNumber;
var group = [];
var isRotatingEndlessly = true;
var isRotatingToHotspot = false;
var isModalOpen = false;
var cameraRadius = 8;
var globeRadius = 1.05;
var closedModalRotation = 0;
var openModalRotation = 0;
var myTimeout;

$(document).ready(function () {
    prepareTopic();
    animate();
    $(".close").on("click", function () {
        isRotatingToHotspot = false;
        isModalOpen = false;
        isRotatingEndlessly = true;
        controls.enableRotate = true;
        controls.update();
        $("#modal").fadeOut(750);
        $("#hideable-info").fadeIn(750);
        $("#canvas").animate({
            "left": 0
        }, 1250);
    });
    $(document).on('click', '.nextButton', function () {
        $('#slideshow').animate({
            scrollLeft: "+=" + ($("#slideshow").width()) * 2.1
        }, 'slow');
    });

    $(document).on('click', '.backButton', function () {
        $('#slideshow').animate({
            scrollLeft: "-=" + ($("#slideshow").width()) * 2.1
        }, 'slow');
    });
});

function moveCamera(radius, lat, long) {
    var xPos = radius * Math.cos(long) * Math.sin(lat);
    var yPos = radius * Math.sin(long) * Math.cos(lat);
    var zPos = radius * Math.cos(lat);
    camera.position.set(xPos, yPos, zPos);
}

function goToHotspot(x) {
    isModalOpen = true;
    modalNumber = x;
    isRotatingToHotspot = true;
    controls.enableRotate = false;
    controls.update();
    $('#slideshow').animate({
        scrollLeft: 0
    }, 'slow');
    $("#modal").fadeIn(750);
    $("#hideable-info").fadeOut(750);
    for (var i = 0; i < group[x].section.length; i++) {
        if (i == 0) {
            $("#slideshow").html("<section id='section0' class='section'><div class='sectionContent'>" + group[x].section[i].information + "</div><div class='slideshow--footer'><button class='nextButton'>MORE</button></div></section>");
        } else if (i == group[x].section.length - 1) {
            $("#slideshow").append("<section id='section" + i + "' class='section'><div class='sectionContent'>" + group[x].section[i].information + "</div><div class='slideshow--footer'><button class='backButton'>BACK</button></div></section>");
        } else {
            $("#slideshow").append("<section id='section" + i + "' class='section'><div class='sectionContent'>" + group[x].section[i].information + "</div><div class='slideshow--footer'><button class='backButton'>BACK</button><button class='nextButton'>MORE</button></div></section>");
        }
    }
    if ($(document).width() >= 700) {
        $("#canvas").animate({
            "left": -($(document).width() / 2.3)
        }, 1250);
    }
    clearTimeout(myTimeout);
}

function prepareTopic() {
    var jsonData = "data/data.json";
    $.getJSON(jsonData, function (data) {
            group = data.topic.slice(0);
        })
        .fail(function () {
            alert("There was an error.");
        })
        .done(function () {
            init();
        });
}

function init() {
    camera = new THREE.PerspectiveCamera(20, window.innerWidth / window.innerHeight, 1, 2000);
    camera.position.x = 5.5;
    camera.position.y = 0;
    camera.position.z = 5.5;
    var ambientLight = new THREE.AmbientLight(0xffffff);
    var pointLight = new THREE.PointLight(0xffffff);
    pointLight.position.x = 30;
    pointLight.position.y = 35;
    pointLight.position.z = 5;
    camera.add(pointLight);
    scene.add(ambientLight);
    scene.add(camera);
    var onProgress = function (xhr) {
        if (xhr.lengthComputable) {
            var percentComplete = xhr.loaded / xhr.total * 100;
            console.log(Math.round(percentComplete, 2) + '% downloaded');
        }
    };
    var onError = function (xhr) {};

    // for globe
    new THREE.MTLLoader()
        .load('assets/earth.mtl', function (materials) {
            materials.preload();
            new THREE.OBJLoader()
                .setMaterials(materials)
                .load('assets/earth.obj', function (object) {
                    obj = object;
                    scene.add(obj);
                }, onProgress, onError);
        });
    // for hotstop
    var marker = new THREE.SphereGeometry(.03, .03, .03);
    for (var i = 0; i < group.length; i++) {
        $("#hideable-info").append("<button id='button" + i + "' class='nav-button' onclick='goToHotspot(" + i + ")'>Go to " + group[i].location + "</button>");
        var object = new THREE.Mesh(marker, new THREE.MeshBasicMaterial({
            color: 0xff7170,
            opacity: 1
        }));
        object.idnum = i;
        var long = group[i].longitude * 0.016;
        var lat = -group[i].latitude * 0.0167 + 1.5;
        object.position.x = Math.sqrt(globeRadius * globeRadius - long * long) * Math.cos(lat);
        object.position.y = long;
        object.position.z = Math.sqrt(globeRadius * globeRadius - long * long) * Math.sin(lat);
        object.scale.x = 1;
        object.scale.y = 1;
        object.scale.z = 1;
        object.rotation.x = 0;
        object.rotation.y = 0;
        object.rotation.z = 0;
        scene.add(object);
    }
    // for location label
    var loader = new THREE.FontLoader();
    loader.load('fonts/helvetiker_regular.typeface.json', function (font) {
        var matLite = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            transparent: false,
            side: THREE.DoubleSide
        });
        for (var i = 0; i < 3; i++) {
            var shapes = font.generateShapes(group[i].location, .05);
            var word = new THREE.ShapeBufferGeometry(shapes);
            word.computeBoundingBox();
            var text = new THREE.Mesh(word, matLite);
            var long = group[i].longitude * 0.016;
            var lat = -group[i].latitude * 0.0167 + 1.5;
            text.rotation.y = 1;
            text.position.x = Math.sqrt(globeRadius * globeRadius - long * long) * Math.cos(lat) + 0.07;
            text.position.y = long;
            text.position.z = Math.sqrt(globeRadius * globeRadius - long * long) * Math.sin(lat);
            scene.add(text);
        }
    });
    //controls
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.enableZoom = true;
    controls.enablePan = false;
    controls.update();
    renderer.setClearColor(0xffffff, 0);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.addEventListener('mousedown', onDocumentMouseDown, false);
    document.addEventListener('mouseup', onDocumentMouseUp, false);
    document.addEventListener('touchstart', onDocumentTouchStart, false);
    window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function onDocumentTouchStart(event) {
    event.preventDefault();
    event.clientX = event.touches[0].clientX;
    event.clientY = event.touches[0].clientY;
    onDocumentMouseDown(event);
}

function onDocumentMouseDown(event) {
    clearTimeout(myTimeout);
    event.preventDefault();
    mouse.x = (event.clientX / renderer.domElement.clientWidth) * 2 - 1;
    mouse.y = -(event.clientY / renderer.domElement.clientHeight) * 2 + 1;
    // for smaller screens
    if ($(document).width() < 700) {
        mouse.y = -(event.clientY / renderer.domElement.clientHeight) * 2 + 1 * 2;
    }
    raycaster.setFromCamera(mouse, camera);
    var intersects = raycaster.intersectObjects(scene.children);
    if (intersects.length > 0) {
        intersects[0].object.material.color.setHex(0x5b0303);
        goToHotspot(intersects[0].object.idnum);
    } else {
        isRotatingEndlessly = false;
    }
}

function onDocumentMouseUp(event) {
    if (!isModalOpen) {
        clearTimeout(myTimeout);
        myTimeout = setTimeout(rotateTrue, 7000);
    }
}

function rotateTrue() {
    isRotatingEndlessly = true;
}

function animate() {
    requestAnimationFrame(animate);
    render();
}

function render() {
    if (isRotatingToHotspot) {
        openModalRotation++;
        moveCamera(cameraRadius, openModalRotation * Math.PI / 50, 0);
        if (openModalRotation % 100 == group[modalNumber].rotateTo) {
            isRotatingToHotspot = false;
            isRotatingEndlessly = false;
        }
    } else if (isRotatingEndlessly) {
        closedModalRotation++;
        moveCamera(cameraRadius, closedModalRotation * Math.PI / 300, 0);
    }
    camera.lookAt(center);
    renderer.render(scene, camera);
}