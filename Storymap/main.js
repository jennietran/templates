$(document).ready(function () {
    init();
    $(".menu button").on("click", function () {
        $(".menu button").removeClass("active");
        var aYear = $(this).attr("id");
        if (isYearInArray(aYear)) {
            location.replace('./index.html?year=' + aYear);
        } else {
            location.replace('./index.html?year=all');
        }
    });
    $("#help").on("click", function () {
        openHelp();
    });
    $("#close").on("click", function () {
        closeHelp();
    });
    $(document).keydown(function (event) {
        if (event.which == 72) {
            if ($('.modal-background').is(':visible')) {
                closeHelp();
            } else {
                openHelp();
            }
        }
        if (event.which == 83) {
            if ($('.modal-background').is(':visible')) {
                closeHelp();
            }
        }
        if (event.which == 83) {
            if ($('.modal-background').is(':visible')) {
                closeHelp();
            }
        }
        if (event.which == 39) { // ->
            $(".vco-layout-landscape .vco-storyslider .vco-slidenav-next").click();
        }
        if (event.which == 37) { // <-
            $(".vco-layout-landscape .vco-storyslider .vco-slidenav-previous").click();
        }
        if (event.which == 9) { // tab
            var year = getYearfromUrlParam();
            var nextYear = (Number(year) + 1).toString();
            console.log(nextYear);
            if (year == "all") {
                $("#2012").click();
            } else if (isYearInArray(nextYear)) {
                $("#"+nextYear+"").click();
            } else {
                $("#all").click();
            }
        }
    });
});


function openHelp() {
    animateCSS('.modal-background', 'slideInDown', function () {
        $(".modal-background").removeClass("animated slideInDown");
    })
    $(".modal-background").show();
}

function closeHelp() {
    animateCSS('.modal-background', 'slideOutUp', function () {
        $(".modal-background").removeClass("animated slideOutUp");
        $(".modal-background").hide();
    })
}

async function init() {
    var year = getYearfromUrlParam();
    var element
    var storymap_data;
    if (isYearInArray(year)) {
        element = document.getElementById(year);
        await $.getJSON('data/data.json', function (data) {
            var newJson = {
                "calculate_zoom": true,
                "line_color": "#AA0CE9",
                "storymap": {
                    "map_type": "stamen:toner-lite",
                    "slides": []
                }
            };
            for (var i = 0; i < data.storymap.slides.length; i++) {
                if (data.storymap.slides[i].date.includes(year) || i == 0) {
                    newJson.storymap.slides.push(data.storymap.slides[i]);
                    if (i == 0) {
                        var title = data.storymap.slides[i].text.headline.split('Timeline')[0];
                        newJson.storymap.slides[i].date = year;
                        newJson.storymap.slides[i].text.headline = title + " Timeline " + year;
                    }
                }
            }
            storymap_data = newJson;
        });
    } else {
        element = document.getElementById("all");
        storymap_data = "data/data.json";
    }
    console.log("make sure you have data", storymap_data)
    element.classList.add("active");
    storymap_options = {};
    storymap = new VCO.StoryMap('mapdiv', storymap_data, storymap_options);
    window.onresize = function (event) {
        storymap.updateDisplay();
    }
}

function getYearfromUrlParam() {
    var url = window.location.search.substring(1);
    var parameters = url.split('&');
    var yearParameter = parameters[0].split('=');
    return yearParameter[1];
}

function isYearInArray(year) {
    var years = ["2012", "2013", "2014", "2015"];
    return ($.inArray(year, years) > -1);
}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }
    node.addEventListener('animationend', handleAnimationEnd)
}