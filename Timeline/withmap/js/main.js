  $(document).ready(function () {
    $.getJSON('./js/timeline-data.json', function (res) {
      console.log(res.foo);
    });
    $('div#modal').hide();
    $(document).on('lity:open', function () {
      $('div#modal').show();
      initMap();
    });
    $(document).on('lity:remove', function () {
      $('div#modal').hide();
    });

    $(document).on("click", ".bottom input", function (event) {
      var t = event.target.id;
      $(this).parent().parent().find('.embed-container').hide();
      $('#' + t + 'c').show();
    });
  });

  // Sets the map on all markers in the array
  function setMapOnAll(map, markers) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

  // Returns slide index by id
  function getSlideIndex(id) {
    var self = this;
    var index = null;
    for (var i = 0; i < self.timeline_json.events.length; i++) {
      if (self.timeline_json.events[i].unique_id === id) {
        index = i;
      }
    }
    return index;
  }

  // Returns current slide id
  function getSlideId() {
    var currentId = window.timeline.getCurrentSlide().data.unique_id;
    return currentId;
  }

  // Returns current slides index
  function getCurrentIndex() {
    var currentId = getSlideId();
    var currentIndex = getSlideIndex(currentId);
    return currentIndex;
  }

  // Returns current slides latLng properties
  function getlatLng() {
    var self = this;
    var currentIndex = getCurrentIndex();
    var location = self.timeline_json.events[currentIndex].latLng;
    return location;
  }

  // Returns title of slide by index
  function getTitle(index) {
    var self = this;
    var title = self.timeline_json.events[index].text.headline;
    return title;
  }

  var ClickEventHandler = function (marker, event) {
    var self = this;
    self.marker = marker;
    self.origin = marker.position;
    self.title = marker.title;
    self.index = marker.index;
    self.map = marker.map;
    self.hasEvent = null;
    // Listen for clicks on the map.
    if (event === 'click') {
      self.marker.addListener(event, self.handleClick.bind(self));
    } else if (event === 'dblclick') {
      self.marker.addListener(event, self.handleDblClick.bind(self));
    }

  };

  // Pan to marker icon on google map
  ClickEventHandler.prototype.goToMarker = function (latLng) {
    var self = this;
    // console.log('Going to event in timeline');
    self.map.setZoom(15);
    self.map.panTo(latLng);
  };

  // Go to slide within timeline
  ClickEventHandler.prototype.goToTimelineEvent = function () {
    var self = this;
    // console.log('Going to event in timeline');
    window.lity.current().close();
    window.timeline.goTo(self.index);
  };

  // Event listener for when marker icon has been clicked
  ClickEventHandler.prototype.handleClick = function (event) {
    var self = this;
    // console.log('You clicked on: ' + event.latLng);
    if (event.latLng) {
      window.setTimeout(function () {
        self.goToMarker(event.latLng);
      }, 200);
    }
  };
  ClickEventHandler.prototype.handleDblClick = function (event) {
    var self = this;
    // console.log('You double clicked on: ' + event.latLng);
    if (event.latLng) {
      self.goToTimelineEvent();
    }
  };


  // Create a pin at speficied index
  function initPins(labelIndex) {
    var pinColor = 'F3B85B';
    var pinImage = new google.maps.MarkerImage("https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + labelIndex.toString() + "|" + pinColor + "|000000",
      new google.maps.Size(21, 34),
      new google.maps.Point(0, 0),
      new google.maps.Point(10, 34));
    return pinImage;
  }

  // Initialize all markers
  function initMarkers(map) {
    var self = this;
    var currentIndex = getCurrentIndex();
    var length = self.timeline_json.events.length;
    var prevIndex = ((currentIndex + length - 1) % length);
    var nextIndex = ((currentIndex + 1) % length);

    var markers = [];

    for (var i = 0; i < self.timeline_json.events.length; ++i) {
      var labelIndex = i + 1;
      var pinImage = initPins(labelIndex);
      var markerPosition = self.timeline_json.events[i].latLng;
      var marker = new google.maps.Marker({
        position: markerPosition,
        map: map,
        icon: pinImage,
        title: getTitle(i),
        zIndex: labelIndex,
        index: labelIndex
      });
      marker.setPosition(markerPosition);
      // Add click event to marker icons
      var clickHandler = new ClickEventHandler(marker, 'click');
      var doubleClickHandler = new ClickEventHandler(marker, 'dblclick');
      clickHandler.hasEvent = true;
      doubleClickHandler.hasEvent = true;
      if (prevIndex === i) {
        marker.zIndex = i + length - 1;
      }
      if (currentIndex === i) {
        marker.zIndex += length;
      }
      if (nextIndex === i) {
        marker.zIndex = i + length + 1;
      }
      // Add marker to markers array
      markers.push(marker);
    }
    // Place markers on map
    setMapOnAll(map, markers);
  }

  // Draw lines from previous to current to next
  function drawLines(map) {
    var self = this;
    var currentIndex = getCurrentIndex();
    var length = self.timeline_json.events.length;
    var prevIndex = ((currentIndex + length - 1) % length);
    var nextIndex = (currentIndex + 1) % length;

    var prevlineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: 0.2,
      scale: 4
    };
    var nextLineSymbol = {
      path: 'M 0,-1 0,1',
      strokeOpacity: 0.4,
      scale: 4
    };
    var prevLine = new google.maps.Polyline({
      path: [self.timeline_json.events[prevIndex].latLng, self.timeline_json.events[currentIndex].latLng],
      strokeOpacity: 0,
      icons: [{
        icon: prevlineSymbol,
        offset: '0',
        repeat: '20px'
      }]
    });
    var nextLine = new google.maps.Polyline({
      path: [self.timeline_json.events[currentIndex].latLng, self.timeline_json.events[nextIndex].latLng],
      strokeOpacity: 0,
      icons: [{
        icon: nextLineSymbol,
        offset: '0',
        repeat: '20px'
      }]
    });
    prevLine.setMap(map);
    nextLine.setMap(map);
  }

  // Initialize map
  function initMap() {
    var latLng = getlatLng();
    var mapOptions = {
      zoom: 15,
      center: latLng,
      mapTypeControlOptions: {
        mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain']
      },
      mapTypeId: 'terrain'
    };
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    initMarkers(map);
    drawLines(map);
  }