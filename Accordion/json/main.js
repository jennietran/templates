(function ($) {
    $.getJSON("assets/data.json", function (data) {
        var items = [];
        $(".heading-primary").html(data.title)
        $(".description").html(data.description)
        $.each(data.slides, function (i, itemI) {
            $("#accordion").append("<div class='group' id='SG" + i +
                "'><button class='accordion-header'><span class='given'>" + itemI.topic +
                "</span></button><div class='accordion-content'></div></div></div"
            );
            $.each(itemI.subtopic, function (j, itemJ) {
                $("#SG" + i + " .accordion-content").append(
                    "<h4>" + itemJ.title +
                    "</h4><div id='g" + i + "sub" + j +
                    "' class='center'></div><p>" +
                    itemJ.text + "</p>"
                );
                if (itemJ.image != "") {
                    $("#g" + i + "sub" + j + ".center").append("<img src='assets/" +
                        itemJ
                        .image +
                        "' alt='" + itemJ.alt + "' width='200'>");
                }
                if (itemJ.video != "") {
                    $("#g" + i + "sub" + j + ".center").append(
                        "<div class='embed-container'><iframe src='" + itemJ
                        .video +
                        "' frameborder='0' allowfullscreen></iframe></div>");
                }
            });
        });
        $('.accordion').accordion({
            collapsible: true,
            active: false,
            heightStyle: 'content',
            autoHeight: false,
            header: '.accordion-header',
            disabled: true
        }).sortable({
            axis: 'y',
            handle: '.accordion-header',
            stop: function (event, ui) {
                ui.item.children('.accordion-header').triggerHandler('focusout');
            }
        });

        $('.accordion-header').on('click', function (e) {
            e.preventDefault();

            $(this).toggleClass('active');
            $(this).next('div.accordion-content').slideToggle(750);
        });
        $(".ui-accordion-header")
            .attr("tabindex", "")
    });
})(jQuery);